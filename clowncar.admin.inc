<?php


/**
 * @file
 * Administration pages for clowncar settings.
 */

/**
 * Menu callback; Listing of all current clowncars.
 */
function clowncar_list() {
  $page = array();

  $clowncars = clowncar_get_clowncars();
	
	
  $page['clowncar_list'] = array(
    '#markup' => theme('clowncar_list', array('clowncars' => $clowncars)),
    '#attached' => array(
      'css' => array(drupal_get_path('module', 'image') . '/image.admin.css' => array()),
    ),
  );

  return $page;

}


/**
 * Returns HTML for the page containing the list of clowncars.
 *
 * @param $variables
 *   An associative array containing:
 *   - clowncars: An array of all the clowncars returned by clowncar_get_clowncars().
 *
 * @ingroup themeable
 */
function theme_clowncar_list($variables) {
  $clowncars = $variables['clowncars'];

  $header = array(t('Clowncar name'), array('data' => t('Operations'), 'colspan' => 3));
  $rows = array();
  foreach ($clowncars as $name => $clowncar) {
    $row = array();
    $row[] = l($name, 'admin/config/media/clowncar/edit/' . $name);
    $link_attributes = array(
      'attributes' => array(
        'class' => array('image-style-link'),
      ),
    );
		
		$row[] = l(t('edit'), 'admin/config/media/clowncar/edit/' . $name, $link_attributes);
		$row[] = l(t('delete'), 'admin/config/media/clowncar/delete/' . $name, $link_attributes);
		
		$rows[] = $row;
	
	}
  
	if (empty($rows)) {
    $rows[] = array(array(
      'colspan' => 4,
      'data' => t('There are currently no clowncars. <a href="!url">Add a new one</a>.', array('!url' => url('admin/config/media/clowncar/add'))),
    ));
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}


/**
 * Form builder; Form for adding a new clowncar.
 *
 * @ingroup forms
 * @see clowncar_add_form_submit()
 * @see clowncar_name_validate()
 */
function clowncar_add_form($form, &$form_state) {
  $form['name'] = array(
    '#type' => 'textfield',
    '#size' => '64',
    '#title' => t('Clowncar name'),
    '#default_value' => '',
    '#description' => t('Used to identify this clowncar when selecting image formats'),
    '#element_validate' => array('clowncar_name_validate'),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create new clowncar'),
  );

  return $form;
}


/**
 * Submit handler for adding a new clowncar.
 */
function clowncar_add_form_submit($form, &$form_state) {
  $clowncar = new stdClass();
	$clowncar->name = $form_state['values']['name'];
  $clowncar = clowncar_save($clowncar);
  drupal_set_message(t('Clowncar %name was created.', array('%name' => $clowncar->name)));
  $form_state['redirect'] = 'admin/config/media/clowncar/edit/' . $clowncar->name;
}


/**
 * Element validate function to ensure unique, URL safe clowncar names.
 */
function clowncar_name_validate($element, $form_state) {
  // @todo: Check for duplicates.

  // Check for illegal characters in name.
  if (preg_match('/[^0-9a-z_\-]/', $element['#value'])) {
    form_set_error($element['#name'], t('Please only use lowercase alphanumeric characters, underscores (_), and hyphens (-) for names.'));
  }
}





/**
 * Form builder; Edit a clowncar name and styles / breakpoints.
 *
 * @param $form_state
 *   An associative array containing the current state of the form.
 * @param $clowncar
 *   A clowncar array, ID or name
 */
function clowncar_edit_form($form, &$form_state, $clowncar) {

	if (isset($clowncar) && !is_array($clowncar)){
	
		$clowncar = is_numeric($clowncar) ? clowncar_load(null, $clowncar) : clowncar_load($clowncar);
	
	}

  $title = t('Edit %name clowncar', array('%name' => $clowncar->name));
  drupal_set_title($title, PASS_THROUGH);

  $form_state['clowncar'] = $clowncar;
  $form['#tree'] = TRUE;
	//$form['#theme'] = 'clowncar_style_list';
  //$form['#attached']['css'][drupal_get_path('module', 'image') . '/image.admin.css'] = array();


	$form['name'] = array(
		'#type' => 'textfield',
		'#size' => '64',
		'#title' => t('Clowncar name'),
		'#default_value' => $clowncar->name,
		'#description' => t('The name is used in URLs for generated images. Use only lowercase alphanumeric characters, underscores (_), and hyphens (-).'),
		'#element_validate' => array('clowncar_name_validate'),
		'#required' => TRUE,
	);
  

  // Build the list of existing styles for this clowncar.
  $form['clowns'] = array(
    '#theme' => 'clowncar_style_list',
  );
	
	$form['clowns']['#clowncar'] = $clowncar;

	if (isset($clowncar->data['clowns']) && is_array($clowncar->data['clowns'])){
		foreach ($clowncar->data['clowns'] as $key => $clown) {
			//$form['clowns'][$key]['#weight'] = isset($form_state['input']['clowns']) ? $form_state['input']['clowns'][$key]['weight'] : NULL;
			$form['clowns'][$key]['label'] = array(
				'#markup' => $clown['style'],
			);
			$form['clowns'][$key]['min_width'] = array(
				'#type' => 'textfield',
				'#title' => t('Min width breakpoint (px)'),
				//'#title_display' => 'invisible',
				'#default_value' => $clown['min-width'],
			);
			$form['clowns'][$key]['max_width'] = array(
				'#type' => 'textfield',
				'#title' => t('Max width breakpoint (px)'),
				//'#title_display' => 'invisible',
				'#default_value' => $clown['max-width'],
			);
			

			
		}
	}

  $form['clowns']['new'] = array(
    '#tree' => FALSE,
    //'#weight' => isset($form_state['input']['weight']) ? $form_state['input']['weight'] : NULL,
  );
  $form['clowns']['new']['new'] = array(
    '#type' => 'select',
    '#title' => t('Style'),
    //'#title_display' => 'invisible',
    '#options' => image_style_options(),
    '#empty_option' => t('Select a new style'),
  );
  $form['clowns']['new']['min_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Min width breakpoint (px)'),
    //'#title_display' => 'invisible',
    '#default_value' => 0,
  );
  $form['clowns']['new']['max_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Max width breakpoint (px)'),
    //'#title_display' => 'invisible',
    '#default_value' => 0,
  );
  $form['clowns']['new']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#validate' => array('clowncar_edit_form_add_validate'),
    '#submit' => array('clowncar_edit_form_submit', 'clowncar_edit_form_add_submit'),
  );

	
	
  // Show the Override or Submit button for this clowncar.
  $form['actions'] = array('#type' => 'actions');

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update clowncar'),
  );

  return $form;
}

/**
 * Returns HTML for the list of clowns in a clowncar.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: the form.
 *
 * @ingroup themeable
 */
function theme_clowncar_style_list($variables) {
	//print_r($variables);
  $clowns = $variables['form'];


  $header = array(t('Style name'), t('Min width'), t('Max width'), array('data' => t('Operations'), 'colspan' => 3));
  $rows = array();
	foreach (element_children($clowns) as $key) {
			$row = array();
		
		if ($key!='new'){
	
			//print_r($clowns[$key]['label']);
			$name = $clowns[$key]['label']['#markup'];
			$row[] = l($name, 'admin/config/media/image-styles/edit/' . $name);
			$link_attributes = array(
				'attributes' => array(
					'class' => array('image-style-link'),
				),
			);

		
		}else{
			$row[] = drupal_render($clowns[$key]['new']);
		
		}
				
		$row[] = drupal_render($clowns[$key]['min_width']);
		$row[] = drupal_render($clowns[$key]['max_width']);
		
		
		$row[] = ($key!='new') ? l(t('remove'), 'admin/config/media/clowncar/edit/' . $clowns['#clowncar']->name . '/remove/' . $key, $link_attributes) : drupal_render($clowns[$key]['add']);
		
		
		$rows[] = $row;
	
	}

	
	
  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Validate handler for adding a new style to a clowncar.
 */
function clowncar_edit_form_add_validate($form, &$form_state) {
  if (!$form_state['values']['new']) {
    form_error($form['clowns']['new']['new'], t('Select a style to add.'));
  }

	if(isset($form_state['values']['max_width']) && !is_numeric($form_state['values']['max_width'])){
    form_error($form['clowns']['new']['max_width'], t('Max width must be numeric.'));
	}
	if(isset($form_state['values']['min_width']) && !is_numeric($form_state['values']['min_width'])){
    form_error($form['clowns']['new']['min_width'], t('Min width must be numeric.'));
	}
	if($form_state['values']['min_width'] && $form_state['values']['max_width'] && ($form_state['values']['min_width'] > $form_state['values']['max_width'])){
		form_error($form['clowns']['new']['min_width'], t('Min width must be less than max width.'));
	}

}

/**
 * Submit handler for adding a new image style to a clowncar.
 */
function clowncar_edit_form_add_submit($form, &$form_state) {
  $clowncar = $form_state['clowncar'];

	$clowncar->data['clowns'][$form_state['values']['new']] = array('style' => $form_state['values']['new'], 'min-width' => (int)$form_state['values']['min_width'], 'max-width' => (int)$form_state['values']['max_width']);
	
	clowncar_save($clowncar);
	

}

/**
 * Submit handler for saving a clowncar.
 */
function clowncar_edit_form_submit($form, &$form_state) {
  // Update the image style name if it has changed.
  $clowncar = $form_state['clowncar'];
  if (isset($form_state['values']['name']) && $clowncar->name != $form_state['values']['name']) {
    $clowncar->name = $form_state['values']['name'];
  }


	
  if ($form_state['values']['op'] == t('Update clowncar')) {
		clowncar_save($clowncar);
    drupal_set_message(t('Changes to the clowncar have been saved.'));
  }
  $form_state['redirect'] = 'admin/config/media/clowncar/edit/' . $clowncar->name;
}


function clowncar_remove_style($clowncar,$style){

	$clowncar = is_numeric($clowncar) ? clowncar_load(null,$clowncar) : clowncar_load($clowncar);
	
	unset($clowncar->data['clowns'][$style]);
	
	clowncar_save($clowncar);
	
	drupal_goto('admin/config/media/clowncar/edit/' . $clowncar->name);

}

/**
 * Form builder; Form for deleting a clowncar.
 *
 * @param $clowncar
 *   A clowncar object.
 *
 * @ingroup forms
 * @see clowncar_delete_form_submit()
 */
function clowncar_delete_form($form, &$form_state, $clowncar) {

	if (isset($clowncar) && !is_array($clowncar)){
	
		$clowncar = is_numeric($clowncar) ? clowncar_load(null, $clowncar) : clowncar_load($clowncar);
	
	}
	
	$form_state['clowncar'] = $clowncar;

  return confirm_form(
    $form,
    t('Sure you want to delete clowncar <em>%clowncar</em>', array('%clowncar' => $clowncar->name)),
    'admin/config/media/clowncar',
    null,
    t('Delete'),  t('Cancel')
  );
}

/**
 * Submit handler to delete a clowncar.
 */
function clowncar_delete_form_submit($form, &$form_state) {
  $clowncar = $form_state['clowncar'];

  clowncar_delete($clowncar);
  drupal_set_message(t('Clowncar %clowncar was deleted.', array('%clowncar' => $clowncar->name)));
  $form_state['redirect'] = 'admin/config/media/clowncar';
}

